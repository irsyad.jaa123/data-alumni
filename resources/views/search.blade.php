{{-- <!doctype html> <html lang="en">
<head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> 
<!--=========== FAV ICON  ===========-->
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
  <link rel="manifest" href="/assets/site.webmanifest">
    <title>search name</title> <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
        crossorigin="anonymous"> </head>
        <body> <div class="container">
            <div class="row"> <div class="col-md-12"> <div class="card mt-4">
                <div class="card-header">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-7">
                                
                                

                    <form action="/search/cari" method="GET">
                        <div class="input-group mb-3">
                        <input type="text" name="nisn" class="form-control" placeholder="Cari (Masukkan NISN)">
                        <br>
                        <select name="kode_jurusan">
                            <option value="">Jurusan</option>
                        @foreach ($kode_jurusan as $kj)
                            <option value="{{ $kj->kode_jurusan }}">
                                {{ $kj->kode_jurusan }}
                            </option>
                        @endforeach
                        </select>
                        <select name="tahun_angkatan">
                            <option value="">Tahun</option>
                        @foreach ($tahun_angkatan as $ta)
                            <option value="{{ $ta->tahun_angkatan }}">
                                {{ $ta->tahun_angkatan }}
                            </option>
                        @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary" href="/search/cari">Search</button>
                </div>
                </form>

            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

            <div class="col-md-12">
                <div class="card-mt-4">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Id Siswa</th>
                                    <th>Nama Siswa</th>
                                    <th>NISN</th>
                                    <th>Alamat</th>
                                    <th>Kontak</th>
                                    <th>Tahun Angkatan</th>
                                    <th>Jurusan</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tbody>
                                @foreach ($data_alumni as $alm)
                                <tr>
                                    <td>{{$alm->id_alumni}}</td>
                                    <td>{{$alm->nama_alumni}}</td>
                                    <td>{{$alm->nisn}}</td>
                                    <td>{{$alm->alamat}}</td>
                                    <td>{{$alm->kontak}}</td>
                                    <td>{{$alm->tahun_angkatan}}</td>
                                    <td>{{$alm->kode_jurusan}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



<h4>
    <a href="/pagee">Kembali</a>
</h4>


            <style>
                .card-header {
                    background-color: lightblue;
                }
            </style>

            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
                crossorigin="anonymous"></script>
            </body>

            </html> --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cari Data</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>

      <!-- header -->
  <header id="home">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark p- bg-dark">
      <div class="container"> <img src="/assets/logo-snapan.png" alt="" class="logoimg"> <a class="navbar-brand"
          href="#">
          Alumni<span style="color:blue;">8</span></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse pe-5" id="navbarNav">
          <div class="mx-auto"></div>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link text-white" href="/pagee">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" href="/search">Cari Data</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" href="/pagee/#berita">Berita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" href="/pagee/#lowongan">Lowongan</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
    


    <div class="container">
              
                        
        <div class="row">
        <div class="col-12">
            <form action="/search/cari" method="GET">
                <div class="input-group mb-3">
                <input type="text" name="nisn" class="form-control" placeholder="Cari (Masukkan NISN)">
                <br>
                <select name="kode_jurusan">
                    <option value="">Jurusan</option>
                @foreach ($kode_jurusan as $kj)
                    <option value="{{ $kj->kode_jurusan }}" style="text-transform: uppercase;">
                        {{ $kj->kode_jurusan }}
                    </option>
                @endforeach
                </select>
                <select name="tahun_angkatan">
                    <option value="">Tahun</option>
                @foreach ($tahun_angkatan as $ta)
                    <option value="{{ $ta->tahun_angkatan }}">
                        {{ $ta->tahun_angkatan }}
                    </option>
                @endforeach
                </select>
                <button type="submit" class="btn" href="/search/cari">Search</button>
        </div>
        </form>
            <div class="card">
                
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Nama</th>
                                    <th scope="col">NISN</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Kontak</th>
                                    <th scope="col">Tahun Angkatan</th>
                                    <th scope="col">Jurusan</th>
                                </tr>
                            </thead>
                            <tbody class="customtable">
                                @foreach ($data_alumni as $alm)
                                <tr>
                                    <td>{{$alm->nama_alumni}}</td>
                                    <td>{{$alm->nisn}}</td>
                                    <td>{{$alm->alamat}}</td>
                                    <td>{{$alm->kontak}}</td>
                                    <td>{{$alm->tahun_angkatan}}</td>
                                    <td style="text-transform: uppercase;">{{$alm->kode_jurusan}}</td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>

       </div> 

       <style>
         @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;500&display=swap');

body {
background-color: rgb(58, 127, 183);
font-family: 'Poppins', sans-serif;
}

.btn{
    background-color: aqua;
}

.container-ta{
margin-top:100px;
}

.row{
    margin-top: 150px;
}
.card {
position: relative;
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
-ms-flex-direction: column;
flex-direction: column;
min-width: 0;
word-wrap: break-word;
background-color: #fff;
background-clip: border-box;
border: 0px solid transparent;
border-radius: 0px;
}



.card-body {
-webkit-box-flex: 1;
-ms-flex: 1 1 auto;
flex: 1 1 auto;
padding: 1.25rem;
}

.card .card-title {
position: relative;
font-weight: 600;
margin-bottom: 10px;
}


.table {
width: 100%;
max-width: 100%;
margin-bottom: 1rem;
background-color: transparent;
}

* {
outline: none;
}

.table th, .table thead th {
font-weight: 500;
}


.table thead th {
vertical-align: bottom;
border-bottom: 2px solid #dee2e6;
}


.table th {
padding: 1rem;
vertical-align: top;
border-top: 1px solid #dee2e6;
}


.table th, .table thead th {
font-weight: 500;
}


th {
text-align: inherit;
}


.m-b-20 {
margin-bottom: 20px;
}


.table td, .table tbody td {
font-weight: 300;
}


.table tbody td {
vertical-align: bottom;
}


.table td {
padding: 1rem;
vertical-align: top;
}


.table td, .table tbody td {
font-weight: 300;
}


td {
text-align: inherit;
}

.nav-item a {
    margin-right: 30px;
    font-family: Poppins;
}

.logoimg {
    width: 50px;
    margin-right: 15px;
}

.carousel-item img {
    height: 100vh;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

.navbar-brand a,
span {
    font-family: Reem Kufi Ink;
}

.navbar{
    padding: 24px;
}

       </style>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>
            