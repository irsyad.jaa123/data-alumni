<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--=========== FAV ICON  ===========-->
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
    <link rel="manifest" href="/assets/site.webmanifest">

    <!--============== LINK CSS ===================-->
    <link rel="stylesheet" href="/css/login.css">

    <title>Login</title>

    <!--======== LINK BOOTSTRAP ===========-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>


    <div class="container">
        <div class="row align-items-center justify-content-center vh-100">
            <div class="col-lg-9">

                <div class="shadow">
                    <div class="row mb-5 mt-5">
                        <div class="col-lg-5">
                            <div class="bg-login h-100"></div>
                        </div>
                        <div class="col-lg-7">
                            <div class="p-5 ps-4 text-dark">
                                <h5 class="mb-1 fw-bold">Selamat Datang Mimin!</h5>
                                <p class="mb-4 text-muted">Silahkan masukkan email dan password</p>
                                <form action="{{ route('adminLogin') }}" method="POST" class="needs-validation" novalidate>
                                    @csrf
                                    <div class="row mb-3">
                                        <div class="col">


                                            <div class="mb-3">
                                                <label for="email" class="form-label">Email Address</label>
                                                <input type="email" class="form-control" id="email" name="email_admin" required autofocus>
                                                {{-- @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif --}}
                                            </div>

                                            <div class="row mb-3">
                                                <div class="col">
                                                    <label for="pass" class="form-label">Password</label>
                                                    <input type="password" class="form-control" id="pass" name="password_admin" required>
                                                    {{-- @if ($errors->has('password'))
                                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                                    @endif --}}
                                                </div>
                                                <div class="mb-3">
                                                    <div class="col d-grid d-inline-block mb-3 mt-4">
                                                        <button type="submit"
                                                            class="btn btn-primary py-2">Login</button>
                                                    </div>
                                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
    <script>

        (() => {
            'use strict'

            const forms = document.querySelectorAll('.needs-validation')

            Array.from(forms).forEach(form => {
                form.addEventListener('submit', event => {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
        })()

    </script>
    <script src="https://kit.fontawesome.com/6dd9914696.js" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
</body>

</html>

































{{-- <!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--=========== FAV ICON  ===========-->
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
    <link rel="manifest" href="/assets/site.webmanifest">

    <!--============== LINK CSS ===================-->
    <link rel="stylesheet" href="/css/login.css">

    <title>Login</title>

    <!--======== LINK BOOTSTRAP ===========-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>


    <div class="container">
        <div class="row align-items-center justify-content-center vh-100">
            <div class="col-lg-9">

                <div class="shadow">
                    <div class="row mb-5 mt-5">
                        <div class="col-lg-5">
                            <div class="bg-login h-100"></div>
                        </div>
                        <div class="col-lg-7">
                            <div class="p-5 ps-4 text-dark">
                                <h5 class="mb-1 fw-bold">Welcome Back!</h5>
                                <p class="mb-4 text-muted">Welcome Back! Please enter your details.</p>
                                <form action="/pagee" method="POST" class="needs-validation" novalidate>
                                    @csrf
                                    <div class="row mb-3">
                                        <div class="col">


                                            <div class="mb-3">
                                                <label for="email" class="form-label">Email Address</label>
                                                <input type="email" class="form-control" id="email" name="email" required autofocus>
                                                @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>

                                            <div class="row mb-3">
                                                <div class="col">
                                                    <label for="pass" class="form-label">Password</label>
                                                    <input type="password" class="form-control" id="password-input" name="password" required>
                                                    @if ($errors->has('password'))
                                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                                    @endif
                                                </div>
                                                <div class="mb-3">
                                                    <div class="col d-grid d-inline-block mb-3 mt-4">
                                                        <button type="submit"
                                                            class="btn btn-primary py-2">Login</button>
                                                    </div>
                                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
    <script>

        (() => {
            'use strict'

            const forms = document.querySelectorAll('.needs-validation')

            Array.from(forms).forEach(form => {
                form.addEventListener('submit', event => {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
        })()

    </script>
    <script src="https://kit.fontawesome.com/6dd9914696.js" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
</body>

</html> --}}