<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--=========== FAV ICON  ===========-->
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
    <link rel="manifest" href="/assets/site.webmanifest">
    
    <title>Survei Alumni</title>
    <link rel="stylesheet" href="/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <div class="form-area">
        <div class="container">
            <div class="row single-form g-0">
                <div class="col-sm-12 col-lg-6">
                    <div class="left">
                        <h2><span>Survei Alumni</span> <br>SMKN 8 Semarang</h2>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="right">
                        <i class="fa fa-caret-left"></i>
                        <form method="post" action="/survei" class="needs-validation" novalidate>
                            @csrf
                            <div class="mb-3">
                                <label for="validationServer03" class="form-label">Apakah anda sekarang melanjutkan kuliah/bekerja sesuai dengan bidang Anda saat di sekolah?</label>
                                <input type="text" class="form-control" name="q1" required>
                                <!-- <div class="invalid-feedback" id="validationServer03Feedback">
                                    eitsss yang ini lupa diisi
                                </div> -->
                                <!-- <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="q1" id="inlineRadio1" value="option1" required>
                                    <label class="form-check-label" for="inlineRadio1">Ya</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="q1" id="inlineRadio1" value="option1" required>
                                    <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                </div> -->
                            </div>
                            
                            <div class="mb-3">
                                {{-- <input type="hidden" value="" class="form-control"  name="q7" required> --}}
                                <input type="hidden" value="{{ $data_survei[0]->id_alumni }}" class="form-control"  name="q8" required>
                            </div>
                            <div class="mb-3">
                                <label for="p-2" class="form-label">Berapa lama waktu yang Anda perlukan untuk mendapatkan pekerjaan setelah lulus?</label>
                                <input type="text" class="form-control"  name="q2" required>
                            </div>
                            <div class="mb-3">
                                <label for="p-3" class="form-label">Bidang apa yang saat ini sedang Anda geluti?</label>
                                <input type="text" class="form-control"  name="q3" required>
                            </div>
                            <div class="mb-3">
                                <label for="p-4" class="form-label">Apakah anda merasa ada perkembangan di sekolah saat ini?</label>
                                <input type="text" class="form-control"  name="q4" required>
                            </div>
                            <div class="mb-3">
                                <label for="p-5" class="form-label">Apakah ada guru yang cara mengajarnya sulit dipahami?</label>
                                <input type="text" class="form-control"  name="q5" required>
                            </div>
                            <div class="mb-3">
                                <label for="p-6" class="form-label">Apakah ada kritik dan saran untuk sekolah ini?</label>
                                <textarea type="text" class="form-control"  name="q6" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary rounded">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
(() => {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }

      form.classList.add('was-validated')
    }, false)
  })
})()
    </script>
  </body>
</html>





























<!-- <!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Survei Alumni</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <div class="col"></div>
        <div class="col mt-3">
            <h5 class="text-center alert alert-primary">Survei Alumni</h5>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5">Apakah anda sekarang bekerja sesuai dengan jurusan anda saat di SMK?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5 ">Apakah motto hidup anda masih sama saat anda berada di SMK?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5 ">Apakah ada kritik dan saran untuk sekolah ini?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5 ">Apakah ada kritik dan saran untuk sekolah ini?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5 ">Apakah ada kritik dan saran untuk sekolah ini?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
        </div>
        <div class="col"></div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html> -->