@extends('layout.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Alumni</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit Alumni</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <body>
        <div class="card">
                 <div class="card-header">
                    Edit Data
                 </div>
                 <div class="card-body">
                    
                    <form action="/data_alumni/update" method="POST">
                        @csrf
                        <input type="hidden" name="id_alumni" value="{{$edit_alumni[0]->id_alumni}}">
                        
                        <div class="mb-3 row">
                          <label for="nama" class="col-sm-2 col-form-label">Email Alumni</label>
                          <div class="col-sm-10">
                             <input type="text" class="form-control" id="email" name="email" value="{{$edit_alumni[0]->email_user}}">
                          </div>
                       </div>
        
                       <div class="mb-3 row">
                          <label for="nama" class="col-sm-2 col-form-label">Nama Alumni</label>
                          <div class="col-sm-10">
                             <input type="text" class="form-control" id="nama" name="nama" value="{{$edit_alumni[0]->nama_alumni}}">
                          </div>
                       </div>
        
                       <div class="mb-3 row">
                          <label for="nisn" class="col-sm-2 col-form-label">NISN</label>
                          <div class="col-sm-10">
                             <input type="text" class="form-control" id="nisn" name="nisn" value="{{$edit_alumni[0]->nisn}}">
                          </div>
                       </div>

                       <div class="mb-3 row">
                        <label for="nis" class="col-sm-2 col-form-label">NIS</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="nis" name="nis" value="{{$edit_alumni[0]->password_user}}">
                        </div>
                     </div>
        
                       <div class="mb-3 row">
                          <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                          <div class="col-sm-10">
                             <input type="text" class="form-control" id="alamat" name="alamat" value="{{$edit_alumni[0]->alamat}}">
                          </div>
                       </div>
        
                       <div class="mb-3 row">
                          <label for="kontak" class="col-sm-2 col-form-label">Kontak</label>
                          <div class="col-sm-10">
                             <input type="text" class="form-control" id="kontak" name="kontak" value="{{$edit_alumni[0]->kontak}}">
                          </div>
                       </div>
        
                       <div class="mb-3 row">
                          <label for="angkatan" class="col-sm-2 col-form-label">Tahun Angkatan</label>
                          <div class="col-sm-10">
                          <select class="form-control" name="angkatan" id="jurusan">
                                <option>{{$edit_alumni[0]->tahun_angkatan}}</option>
                                @foreach ($tahun_angkatan as $ta)
                                    <option value="{{ $ta->tahun_angkatan }}">
                                        {{ $ta->tahun_angkatan }}
                                    </option>
                                @endforeach
                             </select>
                          </div>
                       </div>
        
                       <div class="mb-3 row">
                          <label for="jurusan" class="col-sm-2 col-form-label">Jurusan</label>
                          <div class="col-sm-10">
                          <select class="form-control" name="jurusan" id="jurusan">
                             <option>{{$edit_alumni[0]->kode_jurusan}}</option>
                                @foreach ($kode_jurusan as $kj)
                                    <option value="{{ $kj->kode_jurusan }}">
                                        {{ $kj->kode_jurusan }}
                                    </option>
                                @endforeach
                             </select>
                          </div>
                       </div>
        
                       <div class="col-12">
                          <input type="submit" name="edit" value="Edit Data" class="btn btn-primary">
                       </div>
                     </form>
                 </div>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
        </body>

    
</div>
@endsection