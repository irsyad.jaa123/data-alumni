@extends('layout.admin')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Alumni</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Alumni</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <div class="col-12">
        <div class="card card-primary">
          <div class="card-header">
            <h4 class="card-title">Berita</h4>
          </div>
          <div class="card-body">
            <div class="row">
              @foreach ($data_news as $nws)
              <div class="col-sm-3 mb-3">
                <a href="/assets/img/{{$nws->foto_berita}}" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
                  <img src="/assets/img/{{$nws->foto_berita}}" class="img-fluid mb-2" alt="white sample" style="width: 200px; height:200px;"/>
                </a>
                <h6>{{ $nws->judul_berita }}</h6>
              </div>
              @endforeach
              
              
            </div>
          </div>
        </div>
      </div>

      <div class="col-12">
        <div class="card card-primary">
          <div class="card-header">
            <h4 class="card-title">Lowongan</h4>
          </div>
          <div class="card-body">
            <div class="row">
              @foreach ($data_lowongan as $lwg)
              <div class="col-sm-3 mb-3">
                <a href="/assets/img/{{$lwg->foto_lowongan}}" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
                  <img src="/assets/img/{{$lwg->foto_lowongan}}" class="img-fluid mb-2" alt="white sample" style="width: 200px; height:200px;"/>
                </a>
              </div>
              @endforeach
              
              
            </div>
          </div>
        </div>
      </div>
</div>
@endsection



