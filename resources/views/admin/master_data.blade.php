@extends('layout.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Tambah Data</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah Data</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <!--Memasukkan Data-->
    <div class="card">
      <div class="card-body">
         
         <form action="/master_data" method="POST" autocomplete="off" class="needs-validation" novalidate>
             @csrf
            
            <div class="mb-3 row">
               <label for="nama" class="col-sm-2 col-form-label">Nama Alumni</label>
               <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Lengkap" required>
               </div>
            </div>

            <div class="mb-3 row">
               <label for="email" class="col-sm-2 col-form-label">Email Alumni</label>
               <div class="col-sm-10">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Isi Email" required>
               </div>
            </div>

            <div class="mb-3 row">
               <label for="nisn" class="col-sm-2 col-form-label">NISN</label>
               <div class="col-sm-10">
                  <input type="text" class="form-control" id="nisn" name="nisn" placeholder="Isi NISN" required> 
               </div>
            </div>

            <div class="mb-3 row">
               <label for="nis" class="col-sm-2 col-form-label">NIS</label>
               <div class="col-sm-10">
                  <input type="text" class="form-control" id="nis" name="nis" placeholder="Isi Nama Lengkap" required>
               </div>
            </div>

            <div class="mb-3 row">
               <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
               <div class="col-sm-10">
                  <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Hayo Alamatmu Mana" required>
               </div>
            </div>

            <div class="mb-3 row">
               <label for="kontak" class="col-sm-2 col-form-label">Kontak</label>
               <div class="col-sm-10">
                  <input type="text" class="form-control" id="kontak" name="kontak" placeholder="08 berapa" required>
               </div>
            </div>

            <div class="mb-3 row">
               <label for="angkatan" class="col-sm-2 col-form-label">Tahun Angkatan</label>
               <div class="col-sm-10">
                 <select class="form-control" name="angkatan" id="jurusan" required>
                     <option>Pilih Angkatan</option>
                     @foreach ($tahun_angkatan as $ta)
                         <option value="{{ $ta->tahun_angkatan }}">
                             {{ $ta->tahun_angkatan }}
                         </option>
                     @endforeach
                  </select>
                 </select>
               </div>
            </div>

            <div class="mb-3 row">
               <label for="jurusan" class="col-sm-2 col-form-label">Jurusan</label>
               <div class="col-sm-10">
                  <select class="form-control" name="jurusan" id="jurusan" required>
                  <option>Pilih Jurusan</option>
                     @foreach ($kode_jurusan as $kj)
                         <option value="{{ $kj->kode_jurusan }}">
                             {{ $kj->kode_jurusan }}
                         </option>
                     @endforeach
                  </select>
               </div>
            </div>

            <div class="col-12">
               <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary">
            </div>

         </form>
</div>

<style>
#content {
  width: 100%;
  padding: 20px;
  min-height: 100vh;
  transition: all 0.3s;
}

.mx-auto{
width:780px;
margin-top: 20px;
}

.card{
margin-top:10px;
}
</style>

<script>
   // Example starter JavaScript for disabling form submissions if there are invalid fields
(() => {
'use strict'

// Fetch all the forms we want to apply custom Bootstrap validation styles to
const forms = document.querySelectorAll('.needs-validation')

// Loop over them and prevent submission
Array.from(forms).forEach(form => {
form.addEventListener('submit', event => {
 if (!form.checkValidity()) {
   event.preventDefault()
   event.stopPropagation()
 }

 form.classList.add('was-validated')
}, false)
})
})()
</script>
@endsection