@extends('layout.admin')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Alumni</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Alumni</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <div class="container-fluid">

            {{-- <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
      
                    <div class="info-box-content">
                      <span class="info-box-text">Jumlah Alumni</span>
                      <span class="info-box-number">2,000</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row --> --}}
          
          <!-- /.row -->
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  
  
                  <div class="card-tools">
                    <form action="/data_alumni/cari">
                    <div class="input-group input-group-sm " style="width: 500px;">
                      <input type="text" name="nisn" class="form-control float-right" placeholder="Search">
                      <select name="kode_jurusan">
                        <option value="">Jurusan</option>
                    @foreach ($kode_jurusan as $kj)
                        <option value="{{ $kj->kode_jurusan }}">
                            {{ $kj->kode_jurusan }}
                        </option>
                    @endforeach
                    </select>
                    <select name="tahun_angkatan">
                        <option value="">Tahun</option>
                    @foreach ($tahun_angkatan as $ta)
                        <option value="{{ $ta->tahun_angkatan }}">
                            {{ $ta->tahun_angkatan }}
                        </option>
                    @endforeach
                    </select>
                        <div class="input-group-append">
                          <button type="submit" class="btn btn-default" href="/data_alumni/cari">
                            <i class="fas fa-search"></i>
                          </button>
                        </div>
                      </form>
                      
                    </div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nama Alumni</th>
                        <th>Email</th>
                        <th>NISN</th>
                        <th>NIS</th>
                        <th>Alamat</th>
                        <th>Kontak</th>
                        <th>Angkatan</th>
                        <th>Jurusan</th>
                        <th colspan="2">Aksi</th>
                        <th>Survei</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($data_alumni as $alm)
                                <tr>
                                    <td>{{$alm->id_alumni}}</td>
                                    <td>{{$alm->nama_alumni}}</td>
                                    <td>{{ $alm->email_user }}</td>
                                    <td>{{$alm->nisn}}</td>
                                    <td>{{ $alm->password_user }}</td>
                                    <td>{{$alm->alamat}}</td>
                                    <td>{{$alm->kontak}}</td>
                                    <td>{{$alm->tahun_angkatan}}</td>
                                    <td><span style="text-transform: uppercase">{{$alm->kode_jurusan}}</span></td>
                                    <td><a href="/data_alumni/delete/{{$alm->id_alumni}}" class="text-danger">Delete</a></td>
                                    <td><a href="/data_alumni/tampil/{{$alm->id_alumni}}" class="text-primary">Edit</a></td>
                                    <td><a href="/data_alumni/hasilsurvei/{{ $alm->id_alumni }}"," class="text-info">Lihat Hasil</a></td>
                                </tr>
                                @endforeach
                      
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.row -->
          
         
        </div><!-- /.container-fluid -->
      </section>
</div>
@endsection



{{-- <div class="col-12">
  <div class="card card-primary">
    <div class="card-header">
      <h4 class="card-title">Ekko Lightbox</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/FFFFFF.png?text=1" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/FFFFFF?text=1" class="img-fluid mb-2" alt="white sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/000000.png?text=2" data-toggle="lightbox" data-title="sample 2 - black" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/000000?text=2" class="img-fluid mb-2" alt="black sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/FF0000/FFFFFF.png?text=3" data-toggle="lightbox" data-title="sample 3 - red" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/FF0000/FFFFFF?text=3" class="img-fluid mb-2" alt="red sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/FF0000/FFFFFF.png?text=4" data-toggle="lightbox" data-title="sample 4 - red" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/FF0000/FFFFFF?text=4" class="img-fluid mb-2" alt="red sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/000000.png?text=5" data-toggle="lightbox" data-title="sample 5 - black" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/000000?text=5" class="img-fluid mb-2" alt="black sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/FFFFFF.png?text=6" data-toggle="lightbox" data-title="sample 6 - white" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/FFFFFF?text=6" class="img-fluid mb-2" alt="white sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/FFFFFF.png?text=7" data-toggle="lightbox" data-title="sample 7 - white" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/FFFFFF?text=7" class="img-fluid mb-2" alt="white sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/000000.png?text=8" data-toggle="lightbox" data-title="sample 8 - black" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/000000?text=8" class="img-fluid mb-2" alt="black sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/FF0000/FFFFFF.png?text=9" data-toggle="lightbox" data-title="sample 9 - red" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/FF0000/FFFFFF?text=9" class="img-fluid mb-2" alt="red sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/FFFFFF.png?text=10" data-toggle="lightbox" data-title="sample 10 - white" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/FFFFFF?text=10" class="img-fluid mb-2" alt="white sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/FFFFFF.png?text=11" data-toggle="lightbox" data-title="sample 11 - white" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/FFFFFF?text=11" class="img-fluid mb-2" alt="white sample"/>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="https://via.placeholder.com/1200/000000.png?text=12" data-toggle="lightbox" data-title="sample 12 - black" data-gallery="gallery">
            <img src="https://via.placeholder.com/300/000000?text=12" class="img-fluid mb-2" alt="black sample"/>
          </a>
        </div>
      </div>
    </div>
  </div>
</div> --}}