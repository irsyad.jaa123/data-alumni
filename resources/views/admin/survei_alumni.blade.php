@extends('layout.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Hasil Survei</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Hasil Survei</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <body>
      <div class="card">
               <div class="card-header">
                  Hasil Survei
               </div>
               <div class="card-body">
                @if ($data_survei == "")
                    <div>DATA TIDAK ADA</div>
                @else
                <form action="/data_alumni/update" method="POST">
                  @csrf
                  <input type="hidden" name="id_alumni" value="{{$edit_alumni[0]->id_alumni}}">
  
                 <div class="mb-3 row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama Alumni</label>
                    <div class="col-sm-10">
                       <input type="text" class="form-control" id="nama" name="nama" value="{{$edit_alumni[0]->nama_alumni}}" disabled>
                    </div>
                 </div>
  
                 <div class="mb-3 row">
                    <label for="nisn" class="col-sm-2 col-form-label">NISN</label>
                    <div class="col-sm-10">
                       <input type="text" class="form-control" id="nisn" name="nisn" value="{{$edit_alumni[0]->nisn}}" disabled>
                    </div>
                 </div>

                 @foreach ($data_survei as $sur)
                 <div class="mb-3 row">
                  <label for="nisn" class="col-sm-2 col-form-label">Sesuai Bidang</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="nisn" name="nisn" value="{{$sur->lanjutKB}}" disabled>
                  </div>
               </div>

               <div class="mb-3 row">
                <label for="nisn" class="col-sm-2 col-form-label">Masa Tunggu</label>
                <div class="col-sm-10">
                   <input type="text" class="form-control" id="nisn" name="nisn" value="{{$sur->masaTunggu}}" disabled>
                </div>
             </div>

             <div class="mb-3 row">
              <label for="nisn" class="col-sm-2 col-form-label">Bidang Digeluti</label>
              <div class="col-sm-10">
                 <input type="text" class="form-control" id="nisn" name="nisn" value="{{$sur->bidangKB}}" disabled>
              </div>
           </div>

           <div class="mb-3 row">
            <label for="nisn" class="col-sm-2 col-form-label">Perkembangan</label>
            <div class="col-sm-10">
               <input type="text" class="form-control" id="nisn" name="nisn" value="{{$sur->perkembangan}}" disabled>
            </div>
         </div>

         <div class="mb-3 row">
          <label for="nisn" class="col-sm-2 col-form-label">Guru</label>
          <div class="col-sm-10">
             <input type="text" class="form-control" id="nisn" name="nisn" value="{{$sur->guru}}" disabled>
          </div>
       </div>

       <div class="mb-3 row">
        <label for="nisn" class="col-sm-2 col-form-label">Kritik Saran</label>
        <div class="col-sm-10">
           <input type="text" class="form-control" id="nisn" name="nisn" value="{{$sur->kritikSaran}}" disabled>
        </div>
     </div>
                 @endforeach
                 
                 
                 
         

  
                 
  
                 <div class="col-12">
                    <a href="/data_alumni" class="btn btn-primary"> Kembali</a>
                 </div>
              </form> 
                @endif
                  
                  
               </div>
      </div>
      
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
      </body>
</div>
@endsection