<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--=========== FAV ICON  ===========-->
      <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
      <link rel="manifest" href="/assets/site.webmanifest">
      
    <title>User</title>
</head>
<body>
<div class="">
        <form action="/admin" method="post">
            @csrf
            <label for="">Email admin</label>
            <input type="text" name="email" placeholder="admin@admin.com">
            <label for="">Password Admin </label>
            <input type="password" name="password" id="" placeholder="*****">
            <button type="submit">Simpan</button>
        </form>
    </div>

    <div>
        <table border=1>
            <thead>
                <td>Id</td>
                <td>Email</td>
                <td>Aksi</td>
            </thead>
            <tbody>
                @foreach ($data_admin as $adm)
                    <tr>
                        <td>{{$adm->id_admin}}</td>
                        <td>{{$adm->email_admin}}</td>
                        <td><a href="/admin/delete/{{$adm->id_admin}}">Delete</a></td>
                        <td><a href="/admin/tampil/{{$adm->id_admin}}">Edit</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>    
    </div>
</body>
</html>