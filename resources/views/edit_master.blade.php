<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <!--=========== FAV ICON  ===========-->
     <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
     <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
     <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
     <link rel="manifest" href="/assets/site.webmanifest">

    <title>Edit Alumni</title>
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <style>
      .card{
         margin : 50px 50px
      }

      @media screen and (max-width:720px){
         .mx-auto{
            width:370px;
         }

         .card{
            font-size:9px;
         }
      }
    </style>

</head>
<body>
<div class="card">
         <div class="card-header">
            Edit Data
         </div>
         <div class="card-body">
            
            <form action="/master/update" method="POST">
                @csrf
                <input type="hidden" name="id_alumni" value="{{$edit_alumni[0]->id_alumni}}">
                
                <div class="mb-3 row">
                  <label for="nama" class="col-sm-2 col-form-label">Email Alumni</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="email" name="email" value="{{$edit_alumni[0]->email_user}}">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="nama" class="col-sm-2 col-form-label">Nama Alumni</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="nama" name="nama" value="{{$edit_alumni[0]->nama_alumni}}">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="nisn" class="col-sm-2 col-form-label">NISN</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="nisn" name="nisn" value="{{$edit_alumni[0]->nisn}}">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="alamat" name="alamat" value="{{$edit_alumni[0]->alamat}}">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="kontak" class="col-sm-2 col-form-label">Kontak</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="kontak" name="kontak" value="{{$edit_alumni[0]->kontak}}">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="angkatan" class="col-sm-2 col-form-label">Tahun Angkatan</label>
                  <div class="col-sm-10">
                  <select class="form-control" name="angkatan" id="jurusan">
                        <option>{{$edit_alumni[0]->tahun_angkatan}}</option>
                        @foreach ($tahun_angkatan as $ta)
                            <option value="{{ $ta->tahun_angkatan }}">
                                {{ $ta->tahun_angkatan }}
                            </option>
                        @endforeach
                     </select>
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="jurusan" class="col-sm-2 col-form-label">Jurusan</label>
                  <div class="col-sm-10">
                  <select class="form-control" name="jurusan" id="jurusan">
                     <option>{{$edit_alumni[0]->kode_jurusan}}</option>
                        @foreach ($kode_jurusan as $kj)
                            <option value="{{ $kj->kode_jurusan }}">
                                {{ $kj->kode_jurusan }}
                            </option>
                        @endforeach
                     </select>
                  </div>
               </div>

               <div class="col-12">
                  <input type="submit" name="edit" value="Edit Data" class="btn btn-primary">
               </div>
            
         </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>