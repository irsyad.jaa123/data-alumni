<!-- <html lang="en"> <head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0">
  ===========FAV ICON=========== <link rel="apple-touch-icon" sizes="180x180"
  href="/assets/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png"> <link rel="icon" type="image/png"
  sizes="16x16" href="/assets/favicon-16x16.png"> <link rel="manifest" href="/assets/site.webmanifest"> <link
  rel="stylesheet" href="/css/pagee.css">
<title>SMKN 8 SEMARANG</title> <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
  rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
  crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
    rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
    crossorigin="anonymous"> </head> <body> ==========Navbar=========== <nav class="navbar fixed-top
    navbar-expand-lg navbar-dark p-3"> <div class="container"> <img src="/assets/logo-snapan.png" alt=""
    class="logoimg"> <a class="navbar-brand" href="#">
    Alumni<span style="color:blue;">8</span></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse pe-5" id="navbarNav">
    <div class="mx-auto"></div>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-white" href="home">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="search">Caridata</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="Berita">Berita</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="lowongan">Lowongan</a>
      </li>
    </ul>
    </div>
    </div>
    </nav>
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    </div>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/pi3.jpg" class="d-block " alt="...">
        <div class="carousel-caption ">
          <h5>WELCOME TO ALUMNI8</h5>
          <p>Kebersamaan membuat kita bisa bekerja dengan lebih efektif dan efisien.</p>
        </div>
      </div>
    </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">

      <span class="visually-hidden">Next</span>
    </button>
    </div>


    <main>
      <section class="main-container-left">
        <h2>Top Stories</h2>
        <div class="container-top-left">
          <article>
            <img src="images/top-left.jpg">
    
            <div>
              <h3>Best Used Cars Under $20, 000 for families</h3>
    
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis ea sint, nisi rem earum fugit? Facere
                veritatis sapiente eveniet quibusdam.</p>
    
              <a href="#">Read More <span>>></span></a>
            </div>
          </article>
        </div>
    
        <div class="container-bottom-left">
          <article>
            <img src="images/bottom-left-1.jpg">
            <div>
              <h3>Best smart speakers for the year</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Commodi iure modi animi cupiditate. Explicabo,
                nihil?</p>
    
              <a href="#">Read More <span>>></span></a>
            </div>
          </article>
    
          <article>
            <img src="images/bottom-left-2.jpg">
            <div>
              <h3>iPad Pro, reviewed: Has the iPad become my main computer now?</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Commodi iure modi animi cupiditate. Explicabo,
                nihil?</p>
    
              <a href="#">Read More <span>>></span></a>
            </div>
          </article>
        </div>
      </section>
    
      <section class="main-container-right">
        <h2>Latest Stories</h2>
    
        <article>
          <h4>just in </h4>
          <div>
            <h2>Here's how to track your stimulus check with the IRS Get My Payment Portal</h2>
    
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, repellendus?</p>
    
            <a href="#">Read More <span>>></span></a>
          </div>
          <img src="images/right-1.jpg">
        </article>
    
        <article>
          <h4>just in </h4>
          <div>
            <h2>The best outdoor games to play with your family</h2>
    
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, repellendus?</p>
    
            <a href="#">Read More <span>>></span></a>
          </div>
          <img src="images/right-2.jpg">
        </article>
    
        <article>
          <h4>just in </h4>
          <div>
            <h2>Why walk? Check out the best electric scooters and e-bikes for 2020</h2>
    
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, repellendus?</p>
    
            <a href="#">Read More <span>>></span></a>
          </div>
          <img src="images/right-3.jpg">
        </article>
    
        <article>
          <h4>just in </h4>
          <div>
            <h2>Disneyland Paris will stream its Lion King stage show Friday night</h2>
    
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, repellendus?</p>
    
            <a href="#">Read More <span>>></span></a>
          </div>
          <img src="images/right-4.jpg">
        </article>
    
        <article>
          <h4>just in </h4>
          <div>
            <h2>Looking at a phone's lock screen also requries a warrant, judge rules</h2>
    
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, repellendus?</p>
    
            <a href="#">Read More <span>>></span></a>
          </div>
          <img src="images/right-5.jpg">
        </article>
      </section>
    </main>

    <div class="banner-sub-content">
      <div class="hot-topic">
        <img src="/assets/munif.jpg" alt="" class="img-fluid">

        <div class="hot-topic-content">
          <h2>Waka Kesiswaan SMKN 8 Semarang Benarkan Korban Kecelakaan di Taman Indonesia Kaya Merupakan Siswanya</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non ducimus animi eveniet cumque iste accusamus rerum eligendi expedita asperiores accusantium, quidem nisi enim fuga similique hic impedit nulla repellat repellendus?</p>
          <a href="https://jateng.tribunnews.com/2023/09/22/waka-kesiswaan-smkn-8-semarang-benarkan-korban-kecelakaan-di-taman-indonesia-kaya-merupakan-siswanya">Read More</a>
        </div>
      </div>
    </div> -->

<!-- <div class="wrapper two">
        <div class="neon">
            
        <center><h3>BERITA</h3></center>
        </div>
    </div> -->


<!-- <div class="blog-section"id="Berita">
  <div class="section-content blog">
    <div class="title">
      
    </div>
    <div class="cards">
      @foreach ($data_news as $nws)
      <div class="card">
        <div class="image-section">
          <img src="/assets/img/{{$nws->foto_berita}}" alt="">
        </div>
        <div class="article">
        <p>{{ $nws->judul_berita }}</p>
        </div>
        <div class="blog-view">
        <h6>
      <a class="font-large" href="{{ $nws->link_berita }}">
        {{ $nws->link_berita }}
      </a>
    </h6>
        </div>
        <div class="posted-date">
          <p>Posted on {{ $nws->tanggal_berita }}</p>
        </div>
      </div>
      @endforeach
      {{-- <div class="card">
      <div class="image-section">
        <img src="/assets/ab.jpeg" alt="">
      </div>
      <div class="article">
      <p>Siswa SMKN 8 Semarang Temukan Bug di Google Diganjar Hadiah Puluhan Juta Rupiah</p>
      </div>
      <div class="blog-view">
      <h6>
    <a class="font-large" href="https://edukasi.okezone.com/read/2023/03/07/624/2776734/siswa-smkn-8-semarang-temukan-bug-di-google-diganjar-hadiah-puluhan-juta-rupiah">
    https://edukasi.okezone.com/read/2023/03/07/624/2776734/siswa-smkn-8-semarang-temukan-bug-di-google-diganjar-hadiah-puluhan-juta-rupiah
    </a>
  </h6>
      </div>
      <div class="posted-date">
        <p>posted on oktober 22 2023</p>
      </div>
    </div>
    <div class="card">
      <div class="image-section">
        <img src="/assets/munif.jpg" alt="">
      </div>
      <div class="article">
        <p>Waka Kesiswaan SMKN 8 Semarang Benarkan Korban Kecelakaan di Taman Indonesia Kaya Merupakan Siswanya</p>
      </div>
      <div class="blog-view">
      <h6>
    <a class="font-large" href="https://jateng.tribunnews.com/2023/09/22/waka-kesiswaan-smkn-8-semarang-benarkan-korban-kecelakaan-di-taman-indonesia-kaya-merupakan-siswanya">
    https://jateng.tribunnews.com/2023/09/22/waka-kesiswaan-smkn-8-semarang-benarkan-korban-kecelakaan-di-taman-indonesia-kaya-merupakan-siswanya
    </a>
  </h6>
      </div>
      <div class="posted-date">
        <p>posted on  9 september  2023</p>
      </div>
    </div>
    <div class="card">
      <div class="image-section">
        <img src="/assets/berita11.jpeg" alt="">
      </div>
      <div class="article">
        <p>Mahasiswa Ilkom USM Beri Pelatihan Public Speaking di SMKN 8 Semarang</p>
      </div>
      <div class="blog-view">
      <h6>
    <a class="font-large" href="https://suarabaru.id/2023/05/09/mahasiswa-ilkom-usm-beri-pelatihan-public-speaking-di-smkn-8-semarang">
    https://suarabaru.id/2023/05/09/mahasiswa-ilkom-usm-beri-pelatihan-public-speaking-di-smkn-8-semarang
    </a>
  </h6>
      </div>
      <div class="posted-date">
        <p>posted on  5 september  2023</p>
      </div>
    </div>
    <div class="card">
      <div class="image-section">
        <img src="/assets/berita2.jpg" alt="">
      </div>
      <div class="article">
        <p>Penghubung KY Jateng Bersama Mahasiswa Sosialisasi<br> di SMK Negeri 8 Semarang</p>
      </div>
      <div class="blog-view">
      <h6>
    <a class="font-large" href="https://infopublik.id/kategori/nasional-politik-hukum/727954/penghubung-ky-jateng-bersama-mahasiswa-sosialisasi-di-smk-negeri-8-semarang">
    https://infopublik.id/kategori/<br>nasional-politik-hukum/727954/penghubung-<br>ky-jateng-bersama-mahasiswa-sosialisasi-di<br>-smk-negeri-8-semarang
    </a>
  </h6>
      </div>
      <div class="posted-date">
        <p>posted on 31 maret 2023</p>
      </div>
    </div>
    <div class="card">
      <div class="image-section">
        <img src="/assets/berita4.jpg" alt="">
      </div>
      <div class="article">
        <p> Mahasiswa Ilmu Komunikasi USM, Gelar Seminar Personal Branding di SMKN 8 Semarang</p>
      </div>
      <div class="blog-view">
      <h6>
    <a class="font-large" href=https://sumaterapost.co/mahasiswa-ilmu-komunikasi-usm-gelar-seminar-personal-branding-di-smkn-8-semarang/">
    https://sumaterapost.co/mahasiswa-ilmu-komunikasi-usm-gelar-seminar-personal-branding-di-smkn-8-semarang/
    </a>
  </h6>
    </a>
  </h6>
      </div>
      <div class="posted-date">
        <p>posted on  13 mei 2023</p>
      </div>
      </div> --}}
    </div>
  </div>
</div> -->
<!-- <div class="wrapper two">
        <div class="neon">
            
        <center><h3>lowongan</h3></center>
        </div>
    </div>
<div class="cardd">
  {{-- @foreach ($data_news as $nws)
      <img src="/assets/img/"{{ $nws->$foto_berita }} alt="">
  @endforeach --}}
  {{-- @foreach ($data_lowongan as $lwg)
      <img src="/assets/img/"{{ $lwg->$foto_lowongan }} alt="">
  @endforeach --}}
  {{-- <img src="/assets/lowongan1.jpg">
  <img src="/assets/lowongan2.jpeg">
  <img src="/assets/lowongan3.jpg">
  <img src="/assets/lowongan4.jpg">
  <img src="/assets/lowongan5.png">
  <img src="/assets/lowongan6.jpg"> --}}
  </div>
  </div>
  </div>
  </div>
  </div>
</div> -->
<!-- <footer class="new_footer_area bg_color">
            <div class="new_footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="f_widget company_widget wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;"> -->
<!-- <h3 class="f-title f_600 t_color f_size_18">Get in Touch</h3> -->
<!-- <form action="#" class="f_subscribe_two mailchimp" method="post" novalidate="true" _lpchecked="1">
                                    <p class="mchimp-errmessage" style="display: none;"></p>
                                    <p class="mchimp-sucmessage" style="display: none;"></p>
                                </form>
                            </div>
                        </div>
                       
                        <div class="col-lg-3 col-md-6">
                            <div class="f_widget about-widget pl_70 wow fadeInLeft" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;"> -->
<!-- <h3 class="f-title f_600 t_color f_size_18">Help</h3> -->
<!-- <ul class="list-unstyled f_list">
                                <li><a href="#">Home</a></li>
                                    <li><a href="#">Caridata</a></li>
                                    <li><a href="#">Berita</a></li>
                                    <li><a href="#">Lowongan</a></li>
                                </ul> -->
<!-- </div>
                        </div> -->
<!-- <div class="col-lg-3 col-md-6">
                            <div class="f_widget social-widget pl_70 wow fadeInLeft" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInLeft;">
                                <h3 class="f-title f_600 t_color f_size_18">social media</h3>
                                <div class="f_social_icon">
                                    <a href="#" class="fab fa-facebook"></a>
                                    <a href="#" class="fab fa-twitter"></a>
                                    <a href="#" class="fab fa-instagram"></a>
                                    <a href="#" class="fab fa-pinterest"></a>
                                </div>
                            </div>
                        </div> -->
<!-- </div>
                </div>
                <div class="footer_bg">
                    <div class="footer_bg_one"></div>
                    <div class="footer_bg_two"></div>
                </div>
            </div> -->
<!-- <div class="footer_bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-sm-7">
                            <p class="mb-0 f_400">© SMKN 8 SEMARANG</p>
                        </div>
                        <div class="col-lg-6 col-sm-5 text-right">
                            <p>Made with <i class="icon_heart"></i> in <a href="http://cakecounter.com" target="_blank">CakeCounter</a></p>
                        </div>
                    </div>
                </div>
            </div> -->
<!-- </footer>







    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <script scr="js/bootstrap..bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
      integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
      crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
      var nav = document.querySelector('nav');

      window.addEventListener('scroll', function () {
        if (window.pageYOffset > 100) {
          nav.classList.add('bg-dark', 'shadow', 'opacity-75');
        } else {
          nav.classList.remove('bg-dark', 'shadow', 'opacity-75');
        }
      });

      

        const btnHam = document.querySelector('.ham-btn');
        const btnTimes = document.querySelector('.times-btn');
        const navBar = document.getElementById('nav-bar');

        btnHam.addEventListener('click', function () {
          if (btnHam.className !== "") {
            btnHam.style.display = "none";
            btnTimes.style.display = "block";
            navBar.classList.add("show-nav");
          }
        })

        btnTimes.addEventListener('click', function () {
          if (btnHam.className !== "") {
            this.style.display = "none";
            btnHam.style.display = "block";
            navBar.classList.remove("show-nav");
          }
        })
    </script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
      crossorigin="anonymous"></script>
    </body>

</html> -->

<!DOCTYPE html>
<html class="no-js">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Alumni8</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/pagee.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remixicon/3.5.0/remixicon.css">
  <!-- google fonts -->
  <link
    href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Raleway:wght@300;400;500;700;900&display=swap"
    rel="stylesheet">
  <!-- fontawesome -->
  <script src="https://kit.fontawesome.com/dbed6b6114.js" crossorigin="anonymous"></script>
</head>

<body>

  <!-- header -->
  <header id="home">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark p- bg-dark">
      <div class="container"> <img src="/assets/logo-snapan.png" alt="" class="logoimg"> <a class="navbar-brand"
          href="#">
          Alumni<span style="color:blue;">8</span></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse pe-5" id="navbarNav">
          <div class="mx-auto"></div>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link text-white" href="/pagee">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" href="/search">Cari Data</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" href="#berita">Berita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" href="#lowongan">Lowongan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-danger" href="{{ route('userLogout') }}">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  {{-- <header>
    <nav
    class="navbar navbar-expand-lg navbar-dark shadow fixed-top"
    style="background: linear-gradient(90deg, rgba(24,24,24,1) 49%, rgba(59,170,186,1) 100%);"
  >
    <div class="container">
      <a class="navbar-brand harry fs-1" href="../index.html">Alumni<span style="color: blue">8</span></a>
      <button
        class="navbar-toggler"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-auto fs-6 fw-bold">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#"
              >Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../cerita2/index.html">Cari Data</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../cerita3/index.html">Berita</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../cerita4/index.html">Lowongan</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header> --}}

  <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/assets/hgn.jpg" class="d-block " alt="...">
      @if (session('nama_alumni'))
      <div class="carousel-caption ">
        <h5>Selamat Datang {{ session('nama_alumni') }}</h5>
        <p>Semoga Harimu Baik dan Menyenangkan</p>
        <a href="/survei"><button class="btn btn-primary">Isi Survei</button></a>
      </div>
      @endif
    </div>
  </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
  
    <span class="visually-hidden">Next</span>
  </button>
  </div>


  <main id="berita">
    <section class="main-container-left">
      <h2>Berita Populer</h2>
      <div class="container-top-left">
        <article>
          <img src="/assets/ab.jpeg">

          <div>
            <h3>Siswa SMKN 8 Semarang Temukan Bug di Google Diganjar Hadiah Puluhan Juta Rupiah.</h3>

            <p>Dikutip dari laman resmi SMKN 8 Semarang, Abdullah Mudzakir berhasil mengungkapkan kelemahan sistem keamanan Google di
            balik kecanggihannya tersebut.Siswa yang duduk di kelas XI RPL 2 ini menjadi peretas golongan putih atau yang biasa disebut White Hat Hacker.....</p>

            <a href="https://edukasi.okezone.com/read/2023/03/07/624/2776734/siswa-smkn-8-semarang-temukan-bug-di-google-diganjar-hadiah-puluhan-juta-rupiah">Read More <span>>></span></a>
          </div>
        </article>
      </div>

      <h2 id="lowongan">Lowongan Pekerjaan</h2>

      <div class="container-bottom-left">

        @foreach ($data_lowongan as $lwg)
           <article>
          <img src="/assets/img/{{ $lwg->foto_lowongan }}" style="width: 250px" height="250px">
          <div>
            <a href="https://id.indeed.com/q-it-l-semarang-lowongan.html?vjk=95a6c12f6da34caf">Selengkapnya<span>>></span></a>
          </div>
        </article> 
        @endforeach
        

      </div>
    </section>

    <section class="main-container-right">
        <h2>Berita Lainnya</h2>
        @foreach ($data_news as $nws)
           <article>
        <h4>News</h4>
        <div>
          <h2>{{ $nws->judul_berita }}</h2>

          <p>{{ $nws->isi_berita }}</p>

          <a href="{{ $nws->link_berita }}">Read More <span>>></span></a>
        </div>
        <img src="/assets/img/{{ $nws->foto_berita }}">
      </article> 
        @endforeach
      
    </section>
  </main>

  <footer>
   <div class="footer-content">
          <h3>Alumni<span style="color:blue;">8</span></h3>
          <p>SMK Negeri 8 Semarang, berdiri sejak 1994, mencapai keberhasilan melalui Penddikan Sistem Ganda (PSG) dan prestasi siswa dalam berbagai bidang, dan popularitas yang tinggi.
             Pusat layanan IT, partisipasi aktif siswa dalam lomba dan keterlibatan guru menonjolkan komitmen sekolah terhadap pendidikan yang berkualitas.</p>
          <ul class="socials">
            <li><a href="https://www.facebook.com/smkn8semarang/"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/smk8semarang"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCH1jwRKG1Y90W-pRXcyUqzg"><i class="fa fa-youtube"></i></a></li>
            <li><a href="https://www.instagram.com/humas.smk8semarang/"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>

        <center>
          
        <div class="footer-menu">
          <ul class="f-menu">
            <li><a href="/pagee">Home</a></li>
            <li><a href="/search">Cari Data</a></li>
            <li><a href="#berita">Berita</a></li>
            <li><a href="#lowongan">Lowongan</a></li>
          </ul>
        </div>
        <div class="footer-bottom">
        <p>&copy;copyright Alumni8 2023 </p>
        </div>

        </center>
        
        
  </footer>

  

      <script src="js/bootstrap.bundle.min.js"></script>
      <script>
        var nav = document.querySelector('nav');

        window.addEventListener('scroll', function () {
          if (window.pageYOffset > 100) {
            nav.classList.add( 'shadow', 'opacity-75');
          } else {
            nav.classList.remove( 'shadow', 'opacity-75');
          }
        });



        const btnHam = document.querySelector('.ham-btn');
        const btnTimes = document.querySelector('.times-btn');
        const navBar = document.getElementById('nav-bar');

        btnHam.addEventListener('click', function () {
          if (btnHam.className !== "") {
            btnHam.style.display = "none";
            btnTimes.style.display = "block";
            navBar.classList.add("show-nav");
          }
        })

        btnTimes.addEventListener('click', function () {
          if (btnHam.className !== "") {
            this.style.display = "none";
            btnHam.style.display = "block";
            navBar.classList.remove("show-nav");
          }
        })
      </script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>

</html>