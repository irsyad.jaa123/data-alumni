<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--=========== FAV ICON  ===========-->
      <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
      <link rel="manifest" href="/assets/site.webmanifest">
      
    <title>EDIT DATA</title>
</head>
<body>
<div class="">
        <form action="/admin/update" method="post">
            @csrf
            <input type="hidden" name="id_admin" value="{{$edit_admin[0]->id_admin}}">
            <label for="">Email admin</label>
            <input type="text" name="email" value="{{$edit_admin[0]->email_admin}}">
            <label for="">Password Admin </label>
            <input type="password" name="password" id="" value="{{$edit_admin[0]->password_admin}}">
            <button type="submit" >Edit</button>
        </form>
    </div>

</body>
</html>