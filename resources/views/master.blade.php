<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--=========== FAV ICON  ===========-->
      <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
      <link rel="manifest" href="/assets/site.webmanifest"> 
      
    <title>Master Data</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <style>
        @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";
        body {
            font-family: 'Poppins', sans-serif;
            background: #fafafa;
        }

        p {
            font-family: 'Poppins', sans-serif;
            font-size: 1.1em;
            font-weight: 300;
            line-height: 1.7em;
            color: #999;
        }

        a,
        a:hover,
        a:focus {
            color: inherit;
            text-decoration: none;
            transition: all 0.3s;
        }

        .navbar {
            padding: 15px 10px;
            background: #fff;
            border: none;
            border-radius: 0;
            margin-bottom: 40px;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        }

        .navbar-btn {
            box-shadow: none;
            outline: none !important;
            border: none;
        }

        .line {
            width: 100%;
            height: 1px;
            border-bottom: 1px dashed #ddd;
            margin: 40px 0;
        }

        /* ---------------------------------------------------
            SIDEBAR STYLE
        ----------------------------------------------------- */

        .wrapper {
            display: flex;
            width: 100%;
            align-items: stretch;
        }

        #sidebar {
            min-width: 250px;
            max-width: 250px;
            background: #7386D5;
            color: #fff;
            transition: all 0.3s;
        }

        #sidebar.active {
            margin-left: -250px;
        }

        #sidebar .sidebar-header {
            padding: 20px;
            background: #6d7fcc;
        }

        #sidebar ul.components {
            padding: 20px 0;
            border-bottom: 1px solid #47748b;
        }

        #sidebar ul p {
            color: #fff;
            padding: 10px;
        }

        #sidebar ul li a {
            padding: 10px;
            font-size: 1.1em;
            display: block;
        }

        #sidebar ul li a:hover {
            color: #7386D5;
            background: #fff;
        }

        #sidebar ul li.active>a,
        a[aria-expanded="true"] {
            color: #fff;
            background: #6d7fcc;
        }

        a[data-toggle="collapse"] {
            position: relative;
        }

        .dropdown-toggle::after {
            display: block;
            position: absolute;
            top: 50%;
            right: 20px;
            transform: translateY(-50%);
        }

        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: #6d7fcc;
        }

        ul.CTAs {
            padding: 20px;
        }

        ul.CTAs a {
            text-align: center;
            font-size: 0.9em !important;
            display: block;
            border-radius: 5px;
            margin-bottom: 5px;
        }

        a.download {
            background: #fff;
            color: #7386D5;
        }

        a.article,
        a.article:hover {
            background: #6d7fcc !important;
            color: #fff !important;
        }

        .beranda a{
            color: #fff;
            background: #6d7fcc;
            padding: 10px;
            font-size: 1.1em;
            display: block;
            margin-top:20px;
            border-bottom: 1px solid #47748b;
        }

        /* ---------------------------------------------------
            CONTENT STYLE
        ----------------------------------------------------- */

        #content {
            width: 100%;
            padding: 20px;
            min-height: 100vh;
            transition: all 0.3s;
        }

        .mx-auto{
         width:780px;
         margin-top: 20px;
        }

        .card{
         margin-top:10px;
        }

        /* ---------------------------------------------------
            MEDIAQUERIES
        ----------------------------------------------------- */

        @media (max-width: 768px) {
            #sidebar {
                margin-left: -250px;
            }
            #sidebar.active {
                margin-left: 0;
            }
            #sidebarCollapse span {
                display: none;
            }
        }
    </style>

  </head>
  <body>
    
  <body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Data Alumni</h3>
            </div>

            <div class="beranda">
                <a href="/pagee">Beranda</a>
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Master Data</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="#">Data Alumni</a>
                        </li>
                        <li>
                            <a href="#">Data Angkatan</a>
                        </li>
                        <li>
                            <a href="#">Data Jurusan</a>
                        </li>
                    </ul>
                </li>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>
            </nav>

        <!--Memasukkan Data-->
      <div class="card">
         <div class="card-header">
            Tambah Data
         </div>
         <div class="card-body">
            
            <form action="/master" method="POST">
                @csrf
                <div class="mb-3 row">
                  <label for="nama" class="col-sm-2 col-form-label">Email Alumni</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="email" name="email">
                  </div>
               </div>
                
               <div class="mb-3 row">
                  <label for="nama" class="col-sm-2 col-form-label">Nama Alumni</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Lengkap">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="nisn" class="col-sm-2 col-form-label">NISN</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="nisn" name="nisn" placeholder="Isi NISN">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Hayo Alamatmu Mana">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="kontak" class="col-sm-2 col-form-label">Kontak</label>
                  <div class="col-sm-10">
                     <input type="text" class="form-control" id="kontak" name="kontak" placeholder="08 berapa">
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="angkatan" class="col-sm-2 col-form-label">Tahun Angkatan</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="angkatan" id="jurusan">
                        <option>Pilih Angkatan</option>
                        @foreach ($tahun_angkatan as $ta)
                            <option value="{{ $ta->tahun_angkatan }}">
                                {{ $ta->tahun_angkatan }}
                            </option>
                        @endforeach
                     </select>
                    </select>
                  </div>
               </div>

               <div class="mb-3 row">
                  <label for="jurusan" class="col-sm-2 col-form-label">Jurusan</label>
                  <div class="col-sm-10">
                     <select class="form-control" name="jurusan" id="jurusan">
                     <option>Pilih Jurusan</option>
                        @foreach ($kode_jurusan as $kj)
                            <option value="{{ $kj->kode_jurusan }}">
                                {{ $kj->kode_jurusan }}
                            </option>
                        @endforeach
                     </select>
                  </div>
               </div>

               <div class="col-12">
                  <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary">
               </div>

            </form>

        <!--Mengeluarkan Data-->   

        <div class="card">
         <div class="card-header text-white bg-secondary">
            Data Alumni
         </div>
         <div class="card-body">
            <table class="table">
               <thead>
                  <tr>
                     <th scope="col">Id</th>
                     <th scope="col">Email</th>
                     <th scope="col">Nama Alumni</th>
                     <th scope="col">NISN</th>
                     <th scope="col">Alamat</th>
                     <th scope="col">Kontak</th>
                     <th scope="col">Tahun Angkatan</th>
                     <th scope="col">Jurusan</th>
                     <th scope="col">Aksi</th>
                  </tr>
                  <tbody>
                    @foreach ($data_alumni as $alm)
                    <tr>
                        <td>{{$alm->id_alumni}}</td>
                        <td>{{$alm->email_user}}</td>
                        <td>{{$alm->nama_alumni}}</td>
                        <td>{{$alm->nisn}}</td>
                        <td>{{$alm->alamat}}</td>
                        <td>{{$alm->kontak}}</td>
                        <td>{{$alm->tahun_angkatan}}</td>
                        <td>{{$alm->kode_jurusan}}</td>
                        <td><a href="/master/delete/{{$alm->id_alumni}}" class="text-danger">Delete</a></td>
                        <td><a href="/master/tampil/{{$alm->id_alumni}}" class="text-primary">Edit</a></td>
                    </tr>
                    @endforeach
                  </tbody> 
               </thead>
            </table>
         </div>
      </div>

        </div>
    </div>

    

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>