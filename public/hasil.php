<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Survei Alumni</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <div class="form-area">
        <div class="container">
            <div class="row single-form g-0">
                <div class="col-sm-12 col-lg-6">
                    <div class="kiri">
                        <h2>Terimakasih atas  <span><br>jawaban Anda!</span></h2>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="right">
                        <h4 class="mb-5">Jawaban anda:</h4>
                        <div class="form-item"><p><span>Q:</span> Apakah anda sekarang melanjutkan kuliah/bekerja sesuai dengan bidang Anda saat di sekolah?</p>
                          <p><span>A: </span> <?php echo $_POST['q1']; ?></p>
                        </div>
                        <div class="form-item"><p><span>Q:</span> Berapa lama waktu yang Anda perlukan untuk mendapatkan pekerjaan setelah lulus?</p>
                          <p><span>A: </span> <?php echo $_POST['q2']; ?></p>
                        </div>
                        <div class="form-item"><p><span>Q:</span> Bidang apa yang saat ini sedang Anda geluti?</p>
                          <p><span>A: </span> <?php echo $_POST['q3']; ?></p>
                        </div>
                        <div class="form-item"><p><span>Q:</span> Apakah anda merasa ada perkembangan di sekolah saat ini?</p>
                          <p><span>A: </span> <?php echo $_POST['q4']; ?></p>
                        </div>
                        <div class="form-item"><p><span>Q:</span> Apakah ada guru yang cara mengajarnya sulit dipahami?</p>
                          <p><span>A: </span> <?php echo $_POST['q5']; ?></p>
                        </div>
                        <div class="form-item"><p><span>Q:</span> Apakah ada kritik dan saran untuk sekolah ini?</p>
                          <p><span>A: </span> <?php echo $_POST['q6']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>





























<!-- <!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Survei Alumni</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <div class="col"></div>
        <div class="col mt-3">
            <h5 class="text-center alert alert-primary">Survei Alumni</h5>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5">Apakah anda sekarang bekerja sesuai dengan jurusan anda saat di SMK?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5 ">Apakah motto hidup anda masih sama saat anda berada di SMK?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5 ">Apakah ada kritik dan saran untuk sekolah ini?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5 ">Apakah ada kritik dan saran untuk sekolah ini?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
            <div class="mb-3">
                <label for="p-1" class="form-label fs-5 ">Apakah ada kritik dan saran untuk sekolah ini?</label>
                <input type="textarea" class="form-control" id="p-1">
            </div>
        </div>
        <div class="col"></div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html> -->