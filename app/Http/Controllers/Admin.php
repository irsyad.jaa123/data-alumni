<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Admin extends Controller
{
    public function index()
    {
        $data = DB::table("tbl_admin")->get();
        return view("admin", ["data_admin" => $data]);
    }

    public function save(Request $request)
    {
        $data = $request->all();
        DB::table("tbl_admin")->insert([
            "email_admin" => $data["email"],
            "password_admin" => $data["password"],
        ]);

        return redirect('/admin');

    }

    public function delete($id) {
        DB::table('tbl_admin')->where('id_admin', $id)->delete();
        return redirect('/admin');
    }


    public function tampil(Request $request,$id){
        $v = DB::table('tbl_admin')->where('id_admin', $id)->get();
        // dd($v);

        return view("edit", ["edit_admin" => $v]);
    }

    public function update(Request $request){
        $req = $request->all();
        $v = DB::table('tbl_admin')->where('id_admin', $req['id_admin']);
        $v -> update([
            "email_admin" => $req["email"],
            "password_admin" => $req["password"],
        ]);

        return redirect('/admin');
    }


}
