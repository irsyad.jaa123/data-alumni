<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Data extends Controller
{
    public function index()
    {
        $alumni = DB::table("tbl_alumni")->get();
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        return view("master", ["data_alumni" => $alumni, "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan]);
    }

    public function save(Request $request)
    {
        $alumni = $request->all();
        DB::table("tbl_alumni")->insert([
            "email_user" => $alumni["email"],
            "nama_alumni" => $alumni["nama"],
            "nisn" => $alumni["nisn"],
            "alamat" => $alumni["alamat"],
            "kontak" => $alumni["kontak"],
            "kode_jurusan" => $alumni["jurusan"],
            "tahun_angkatan" => $alumni["angkatan"]
        ]);

        return redirect('/master');
    }

    public function delete($id) {
        DB::table('tbl_alumni')->where('id_alumni', $id)->delete();
        return redirect('/master');
    }

    public function tampil(Request $request,$id){
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        $v = DB::table('tbl_alumni')->where('id_alumni', $id)->get();
        // dd($v);

        return view('edit_master', ["edit_alumni" => $v,  "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan]);
    }

    public function update(Request $request){
        $req = $request->all();
        $v = DB::table('tbl_alumni')->where('id_alumni', $req['id_alumni']);
        $v -> update([
            "email_user" => $req["email"],
            "nama_alumni" => $req["nama"],
            "nisn" => $req["nisn"],
            "alamat" => $req["alamat"],
            "kontak" => $req["kontak"],
            "kode_jurusan" => $req["jurusan"],
            "tahun_angkatan" => $req["angkatan"]
        ]);

        return redirect('/master');
    }
}
