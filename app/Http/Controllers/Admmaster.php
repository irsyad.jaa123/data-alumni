<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Admmaster extends Controller
{
    public function index()
    {
        $alumni = DB::table("tbl_alumni")->get();
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        return view("/admin/master_data", ["data_alumni" => $alumni, "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan]);
    }

    public function save(Request $request)
    {
        $alumni = $request->all();
        DB::table("tbl_alumni")->insert([
            "email_user" => $alumni["email"],
            "nama_alumni" => $alumni["nama"],
            "nisn" => $alumni["nisn"],
            "password_user" => $alumni["nis"],
            "alamat" => $alumni["alamat"],
            "kontak" => $alumni["kontak"],
            "kode_jurusan" => $alumni["jurusan"],
            "tahun_angkatan" => $alumni["angkatan"]
        ]);

        return redirect('/master_data');
    }
}
