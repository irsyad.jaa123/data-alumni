<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Admdata extends Controller
{
    public function index(Request $request)
    {
        
        $jumlah = DB::table("tbl_alumni")->get();
        $data = DB::table("tbl_alumni")
        ->get();
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        $survei = DB::table("tbl_survei")
        ->join('tbl_alumni','tbl_alumni.id_alumni', '=', 'tbl_survei.id_alumni')
        ->get(['tbl_survei.lanjutKB', 'tbl_survei.masaTunggu', 'tbl_survei.perkembangan', 'tbl_survei.guru', 'tbl_survei.kritikSaran' ]);
        return view("/admin/data_alumni", ["data_alumni" => $data, "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan, "data_survei" => $survei, "jumlah" => $jumlah]);

    }

    public function save(Request $request)
    {
        $alumni = $request->all();
        DB::table("tbl_alumni")->insert([
            "email_user" => $alumni["email"],
            "nama_alumni" => $alumni["nama"],
            "nisn" => $alumni["nisn"],
            "alamat" => $alumni["alamat"],
            "kontak" => $alumni["kontak"],
            "kode_jurusan" => $alumni["jurusan"],
            "tahun_angkatan" => $alumni["angkatan"]
        ]);

        return redirect('/admin/data_alumni');
    }

    public function cari(Request $request)
    {
        $req = $request->all();
        $nisn = '%'. $req ['nisn'].'%';
        $kode_jurusan = '%'. $req ['kode_jurusan'].'%';
        $tahun_angkatan = '%'. $req ['tahun_angkatan'].'%';
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        $data = DB::table('tbl_alumni')->where('nisn', 'like', $nisn)
        ->where('kode_jurusan', 'like', $kode_jurusan)
        ->where('tahun_angkatan', 'like', $tahun_angkatan)
        // ->orWhere('tahun_angkatan', $req['tahun_angkatan'], 'kode_jurusan', $req['kode_jurusan'])
        // ->orWhere('tahun_angkatan', $req['tahun_angkatan'])
        // ->orWhere('kode_jurusan', $req['kode_jurusan'])
        ->get();
        return view("/admin/data_alumni", ["data_alumni" => $data, "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan]);
    }
    
    public function delete($id) {
        DB::table('tbl_alumni')->where('id_alumni', $id)->delete();
        return redirect('/data_alumni');
    }

    public function tampil(Request $request,$id){
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        $v = DB::table('tbl_alumni')->where('id_alumni', $id)->get();
        // dd($v);

        return view('/admin/edit_admin', ["edit_alumni" => $v,  "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan]);
    }

    public function update(Request $request){
        $req = $request->all();
        $v = DB::table('tbl_alumni')->where('id_alumni', $req['id_alumni']);
        $v -> update([
            "email_user" => $req["email"],
            "nama_alumni" => $req["nama"],
            "nisn" => $req["nisn"],
            "password_user" => $req["nis"],
            "alamat" => $req["alamat"],
            "kontak" => $req["kontak"],
            "kode_jurusan" => $req["jurusan"],
            "tahun_angkatan" => $req["angkatan"]
        ]);

        return redirect('/data_alumni');
    }

    public function hasilsurvei(Request $request,$id){
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        $survei = DB::table("tbl_survei")
        ->leftJoin('tbl_alumni','tbl_alumni.id_alumni', '=', 'tbl_survei.id_alumni')
        ->where('tbl_survei.id_alumni','=',$id)
        ->get(['tbl_alumni.nisn','tbl_alumni.nama_alumni','tbl_survei.lanjutKB', 'tbl_survei.masaTunggu', 'tbl_survei.perkembangan', 'tbl_survei.guru', 'tbl_survei.kritikSaran', 'tbl_survei.bidangKB' ]);
        $v = DB::table('tbl_alumni')->where('id_alumni', $id)->get();
        // dd($v);
        

        return view('/admin/survei_alumni', ["edit_alumni" => $v,  "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan, "data_survei" => $survei]);
    }
    
    
}
