<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Admnews extends Controller
{
    public function index()
    {
        $news = DB::table("tbl_berita")->get();
        $lowongan = DB::table("tbl_lowongan")->get();
        return view("/admin/foto", ["data_news" => $news, "data_lowongan" => $lowongan]);
    }
}
