<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Session;

class Login extends Controller
{
    
    public function beritaLowongan()
    {
        $news = DB::table("tbl_berita")->get();
        $lowongan = DB::table("tbl_lowongan")->get();
        return view("pagee", ["data_news" => $news, "data_lowongan" => $lowongan]);
    }
    
    public function index(Request $request)
    {
        if(!empty(DB::table('tbl_alumni', $request->session()->get('email_user'))->first())) {
            $login = DB::table('tbl_alumni', $request->session()->get('email_user'))->first();

            view('pagee')->with('email_user', $login->email_user);

            return view("login")->with([
                'user' => $login
            ]);
        }


        // return view("pagee");
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email_user' => 'required|email',
            'password_user' => 'required',
        ]);


        $login = DB::table('tbl_alumni')->where('email_user',$request->email_user)->first();
        
        if(!empty($login)) {
            // dd($login);
            // dd($request->email_user,$login->password_user,  $request->password_user);
            if($login->password_user ==  $request->password_user) {
                $request->session()->put('nama_alumni', $login->nama_alumni);
                $request->session()->put('email_user', $login->email_user);
                // $request->session()->put('password_user', $login->password_user);
                return redirect('/pagee');
            }
        }

        return view('login');
    }

    public function logout(Request $request) 
    {
        $request->session()->flush();
        // $request->session()->forget('email_user');

        return redirect('/login');
    }

    // ADMIN

    public function indexmin(Request $request)
    {
        if(!empty(DB::table('tbl_admin', $request->session()->get('email_admin'))->first())) {
            $admin = DB::table('tbl_admin', $request->session()->get('email_admin'))->first();

            view('admin\data_alumni')->with('email_admin', $admin->email_admin);

            return view("login_admin")->with([
                'adm' => $admin
            ]);
        }


        // return view("pagee");
    }

    public function postLoginAdmin(Request $request)
    {
        // $this->validate($request, [
        //     'email_user' => 'required|email',
        //     'password_user' => 'required',
        // ]);


        $admin = DB::table('tbl_admin')->where('email_admin',$request->email_admin)->first();
        
        if(!empty($admin)) {
            // dd($admin);
            // dd($request->email_admin,$admin->password_admin,  $request->password_admin);
            if($admin->password_admin ==  $request->password_admin) {
                // $request->session()->put('nama_alumni', $admin->nama_alumni);
                // $request->session()->put('email_admin', $admin->email_admin);
                // $request->session()->put('password_user', $admin->password_user);
                return redirect('/data_alumni');
            }
        }

        return view('login_admin');
    }

    public function logoutmin(Request $request) 
    {
        $request->session()->flush();
        // $request->session()->forget('email_user');

        return redirect('/login_admin');
    }
    
    



    // public function customLogin(Request $request)
    // {
    //     $request->validate([
    //         'email' => ['required', 'email'],
    //         'password' => ['required', 'numeric']
    //     ]);
    //     $req = $request->all();
    //     $email = $req[''];
    //     $pass = $req[''];

    //     $user = DB::table("tbl_user")->where('email_user, password_user, nisn')->get();

    //     if ($user == 0){
    //         return redirect("/pagee")->withSuccess('Login details are not valid');
    //     }

    //     if ($user['password_user'] != $pass ) {
    //         return redirect("/pagee")->withSuccess('Login details are not valid');
    //     }

        // session
        // user id
        // email
        // nisn

        // if($request->session()->has('id_user')){
		// 	echo $request->session()->get('id_user');
		// }else{
		// 	echo 'Tidak ada data dalam session.';
		// }

        // if($request->session()->has('email_user')){
		// 	echo $request->session()->get('email_user');
		// }else{
		// 	echo 'Tidak ada data dalam session.';
		// }

        // if($request->session()->has('nisn')){
		// 	echo $request->session()->get('nisn');
		// }else{
		// 	echo 'Tidak ada data dalam session.';
		// }

        // return redirect("/dashboard");

    
        // $credentials = $request->only('email', 'password');
        // if (Auth::attempt($credentials)) {
        //     return redirect()->intended('pagee')
        //                 ->withSuccess('Signed in');
        // }
   
        // return redirect("/pagee")->withSuccess('Login details are not valid');
    // }



    // public function pagee()
    // {
    //     if(Auth::check()){
    //         return view('pagee');
    //     }
   
    //     return redirect("/survei")->withSuccess('You are not allowed to access');
    // }
}