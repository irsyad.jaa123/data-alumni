<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Search extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table("tbl_alumni")
        ->get();
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        return view("search", ["data_alumni" => $data, "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan]);

    }

    public function cari(Request $request)
    {
        $req = $request->all();
        $nisn = '%'. $req ['nisn'].'%';
        $kode_jurusan = '%'. $req ['kode_jurusan'].'%';
        $tahun_angkatan = '%'. $req ['tahun_angkatan'].'%';
        $data_jurusan = DB::table("jurusan")
        ->get();
        $data_angkatan = DB::table("angkatan")
        ->get();
        $data = DB::table('tbl_alumni')->where('nisn', 'like', $nisn)
        ->where('kode_jurusan', 'like', $kode_jurusan)
        ->where('tahun_angkatan', 'like', $tahun_angkatan)
        // ->orWhere('tahun_angkatan', $req['tahun_angkatan'], 'kode_jurusan', $req['kode_jurusan'])
        // ->orWhere('tahun_angkatan', $req['tahun_angkatan'])
        // ->orWhere('kode_jurusan', $req['kode_jurusan'])
        ->get();
        return view("/search", ["data_alumni" => $data, "kode_jurusan" => $data_jurusan, "tahun_angkatan"=> $data_angkatan]);
    }
}
