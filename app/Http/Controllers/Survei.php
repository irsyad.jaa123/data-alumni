<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Survei extends Controller
{
    public function index(Request $request)
    {
        // $survei = DB::table("tbl_survei")->get();
        $email = $request->session()->get('email_user');
        // $user = DB::table("tbl_user")->where('email_user', $email)->get();

        // dd($email);
        $alumni = DB::table("tbl_alumni")->where('email_user', $email)->get();


        return view("survei", ["data_survei" => $alumni]);
    }

    public function save(Request $request)
    {
        $survei = $request->all();
        DB::table("tbl_survei")->insert([
            "lanjutKB" => $survei["q1"],
            "masaTunggu" => $survei["q2"],
            "bidangKB" => $survei["q3"],
            "perkembangan" => $survei["q4"],
            "guru" => $survei["q5"],
            "kritikSaran" => $survei["q6"],
            // "id_admin" => $survei["q7"],
            "id_alumni" => $survei["q8"]
        ]);

        return redirect('/pagee');
    }
}