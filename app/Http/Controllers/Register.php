<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Register extends Controller
{
    public function index()
    {
        $alumni = DB::table("tbl_alumni")->get();
        return view("register", ['data_alumni'=>$alumni]);
    }

    public function save(Request $request)
    {
        $alumni = $request->all();
        DB::table("tbl_alumni")->insert([
            "nama_alumni" => $alumni["p1"],
            "nisn" => $alumni["p2"],
            "alamat" => $alumni["p5"],
            "kontak" => $alumni["p6"],
            "tahun_angkatan" => $alumni["p4"],
            "jurusan" => $alumni["p3"],
        ]);

        return redirect('/pagee');
    }
}
