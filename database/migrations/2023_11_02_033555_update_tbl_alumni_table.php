<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_alumni', function (Blueprint $table) {
            $table->enum('kode_jurusan',['pplg','tjkt','dkv','ps','lk','rpl','mm','tkj','cg']);
            $table->enum('tahun_angkatan',['2018','2019','2020','2021','2022','2023']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
