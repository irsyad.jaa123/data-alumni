<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\Login;
use App\Http\Controllers\Survei;
use App\Http\Controllers\Data;
use App\Http\Controllers\Search;
use App\Http\Controllers\Register;
use App\Http\Controllers\Berita;
use App\Http\Controllers\Lowongan;
use App\Http\Controllers\Admdata;
use App\Http\Controllers\Admmaster;
use App\Http\Controllers\Admnews;
use App\Http\Controllers\Admsurvei;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get("/admin", [Admin::class, "index"]);
Route::get("/admin/delete/{id}", [Admin::class, "delete"]);
Route::post("/admin", [Admin::class, "save"]);
Route::get("/admin/tampil/{id}", [Admin::class, "tampil"]);
Route::post("/admin/update", [Admin::class, "update"]);




// Route::get("/pagee", [Lowongan::class, "index"]);
Route::get("/pagee", [Login::class, "beritaLowongan"]);
Route::get('/login', [Login::class, 'index'])->name('login');
Route::post('/postLogin', [Login::class, 'postLogin'])->name('userLogin');
Route::get('/logout', [Login::class, 'logout'])->name('userLogout');
Route::get('/login_admin', [Login::class, 'indexmin'])->name('login_admin');
Route::post('/postLoginAdmin', [Login::class, 'postLoginAdmin'])->name('adminLogin');
Route::get('/logoutmin', [Login::class, 'logoutmin'])->name('adminLogout');
// Route::get('/session/tampil', [Login::class, "tampilkanSession"]);
// Route::post('custom-login', [Login::class, 'customLogin'])->name('login.custom');
// Route::post('pagee', [Login::class, 'pagee']);

Route::get("/register", [Register::class, "index"]);
Route::post("/register", [Register::class, "save"]);

Route::get("/survei", [Survei::class, "index"]);
Route::post("/survei", [Survei::class, "save"]);

Route::get("/master", [Data::class, "index"]);
Route::post("/master", [Data::class, "save"]);
Route::get("/master/delete/{id}", [Data::class, "delete"]);
Route::get("/master/tampil/{id}", [Data::class, "tampil"]);
Route::post("/master/update", [Data::class, "update"]);

Route::get("/search", [Search::class, "index"]);
Route::get("/search/cari", [Search::class, "cari"]);
Route::get("/search/jurusan", [Search::class, "jurusan"]);

Route::get("/data_alumni", [Admdata::class, "index"]);
Route::get("/data_alumni/cari", [Admdata::class, "cari"]);
Route::get("/data_alumni/jurusan", [Admdata::class, "jurusan"]);
Route::get("/data_alumni/delete/{id}", [Admdata::class, "delete"]);
Route::get("/data_alumni/tampil/{id}", [Admdata::class, "tampil"]);
Route::post("/data_alumni/update", [Admdata::class, "update"]);
Route::get("/data_alumni/hasilsurvei/{id}", [Admdata::class, "hasilsurvei"]);

Route::get('/', function () {
    return view('welcome');
});


Route::get("/master_data", [Admmaster::class, "index"]);
Route::post("/master_data", [Admmaster::class, "save"]);

Route::get("/foto", [Admnews::class, "index"]);

Route::get('/survei_alumni', function () {
    return view('/admin/survei_alumni');
});




